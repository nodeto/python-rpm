Requires:       tzdata
Requires:       bash
Requires:       chkconfig
Requires:       glibc
Requires:       libxcrypt
Requires:       openssl-libs
Requires:       expat
Requires:       libffi
Requires:       gdbm-libs
Requires:       xz-libs
Requires:       ncurses-libs
Requires:       libnsl2
Requires:       bzip2-libs
Requires:       libtirpc
Requires:       libuuid
Requires:       readline
Requires:       sqlite-libs
Requires:       zlib

BuildRequires: autoconf
BuildRequires: bluez-libs-devel
BuildRequires: bzip2
BuildRequires: bzip2-devel
BuildRequires: desktop-file-utils
BuildRequires: expat-devel

BuildRequires: findutils
BuildRequires: gcc-c++
BuildRequires: gdbm-devel
BuildRequires: git-core
BuildRequires: glibc-all-langpacks
BuildRequires: glibc-devel
BuildRequires: gmp-devel
BuildRequires: gnupg2
BuildRequires: libappstream-glib
BuildRequires: libffi-devel
BuildRequires: libnsl2-devel
BuildRequires: libtirpc-devel
BuildRequires: libGL-devel
BuildRequires: libuuid-devel
BuildRequires: make
BuildRequires: ncurses-devel

BuildRequires: openssl-devel
BuildRequires: pkgconfig
BuildRequires: readline-devel
BuildRequires: redhat-rpm-config
BuildRequires: sqlite-devel
BuildRequires: gdb

BuildRequires: tar
BuildRequires: tcl-devel
BuildRequires: tix-devel
BuildRequires: tk-devel
BuildRequires: tzdata

BuildRequires: valgrind-devel

BuildRequires: xz-devel
BuildRequires: zlib-devel

BuildRequires: /usr/bin/dtrace

# workaround http://bugs.python.org/issue19804 (test_uuid requires ifconfig)
BuildRequires: /usr/sbin/ifconfig

# Require alternatives version that implements the --keep-foreign flag
Requires:         alternatives >= 1.19.1-1
Requires(post):   alternatives >= 1.19.1-1
Requires(postun): alternatives >= 1.19.1-1
