[ "$RPM_BUILD_ROOT" != "/" ] && rm -rf $RPM_BUILD_ROOT
make DESTDIR=$RPM_BUILD_ROOT altinstall

# Switch all shebangs to refer to the specific Python version.
# This currently only covers files matching ^[a-zA-Z0-9_]+\.py$,
# so handle files named using other naming scheme separately.
LD_LIBRARY_PATH=%{buildroot} \
  /usr/libexec/platform-python \
  %{_builddir}/Python-%{version}/Tools/scripts/pathfix.py \
  -i "/usr/local/bin/python%{pybasever}" -pn \
  %{buildroot}

# Check everything in the bin directory as well (grep used to find non-binary files)
find %{buildroot}/usr/local/bin/ -type f -exec grep -Iq . {} \; -print | \
  xargs sed -i 's/#!\/usr\/local\/bin\/python$/#!\/usr\/local\/bin\/python3.11/g'

# Remove shebang lines from .py files that aren't executable, and
# remove executability from .py files that don't have a shebang line:
find %{buildroot} -name \*.py \
  \( \( \! -perm /u+x,g+x,o+x -exec sed -e '/^#!/Q 0' -e 'Q 1' {} \; \
  -print -exec sed -i '1d' {} \; \) -o \( \
  -perm /u+x,g+x,o+x ! -exec grep -m 1 -q '^#!' {} \; \
  -exec chmod a-x {} \; \) \)


mkdir -p %{buildroot}%{_bindir} && chmod 755 %{buildroot}%{_bindir}

#delete man files -- keep system files in place
rm -rf %{buildroot}/usr/local/share

#add ldconf file
mkdir -p %{buildroot}/etc/ld.so.conf.d
echo "/usr/local/lib" > %{buildroot}/etc/ld.so.conf.d/python%{pybasever}.conf

#remove python3 link so not to conflict with system python
rm -f %{buildroot}%{_bindir}/python3

%clean
[ "$RPM_BUILD_ROOT" != "/" ] && rm -rf $RPM_BUILD_ROOT
