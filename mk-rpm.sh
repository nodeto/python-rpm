#!/bin/bash

podman build . -t python-build | tee build-log.txt
podman run --rm -d --name python-build python-build sleep 300
podman cp python-build:/root/rpmbuild/RPMS/ .
podman kill python-build

