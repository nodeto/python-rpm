./configure \
  --enable-loadable-sqlite-extensions \
  --enable-optimizations \
  --enable-option-checking=fatal \
  --enable-shared \
  --with-lto \
  --with-ssl-default-suites=openssl \
  --with-builtin-hashlib-hashes=blake2 \
  --without-ensurepip

nproc="$(nproc)"
nproc="$((nproc * 2 + 1))"
make -j "$nproc"
