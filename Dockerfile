ARG ROCKY_VERSION=9
FROM docker.io/rockylinux/rockylinux:${ROCKY_VERSION} as buildenv

ARG ROCKY_VERSION=9
ARG PYTHON_VER=3.11.1

RUN dnf install -y \
    yum-utils \
    && dnf clean all

RUN yum-config-manager \
    --setopt=baseos.baseurl=https://nexus.nodeto.site/repository/rocky/${ROCKY_VERSION}/BaseOS/x86_64/os \
    --setopt=appstream.baseurl=https://nexus.nodeto.site/repository/rocky/${ROCKY_VERSION}/AppStream/x86_64/os \
    --setopt=extras.baseurl=https://nexus.nodeto.site/repository/rocky/${ROCKY_VERSION}/extras/x86_64/os \
    --setopt=devel.baseurl=https://nexus.nodeto.site/repository/rocky/${ROCKY_VERSION}/devel/x86_64/os \
    --setopt=baseos.mirrorlist= \
    --setopt=appstream.mirrorlist= \
    --setopt=extras.mirrorlist= \
    --setopt=devel.mirrorlist= \
    --save

RUN yum-config-manager --enable devel

RUN dnf install -y \
    rpm-build \
    rpmdevtools \
    dnf-utils \
    yum-utils \
    && dnf clean all

RUN rpmdev-setuptree

RUN curl https://www.python.org/ftp/python/${PYTHON_VER}/Python-${PYTHON_VER}.tar.xz \
    -o /root/rpmbuild/SOURCES/Python-${PYTHON_VER}.tar.xz

COPY python311.spec /root/rpmbuild/SPECS/
COPY requires.spec /root/rpmbuild/SPECS/
COPY noop.spec /root/rpmbuild/SPECS/build.spec
COPY noop.spec /root/rpmbuild/SPECS/install.spec
RUN yum-builddep -y /root/rpmbuild/SPECS/python311.spec

COPY build.spec /root/rpmbuild/SPECS/
COPY install.spec /root/rpmbuild/SPECS/
RUN rpmbuild -ba --noclean --short-circuit  /root/rpmbuild/SPECS/python311.spec
