%define pybasever 3.11
%define version %{pybasever}.1

Name:           nodeto.com-python311
Version:        %{version}
Release:        1%{?dist}
Summary:        Python %{version} interpreter
BuildArch:      x86_64

License:        GPL
Source0:        https://www.python.org/ftp/python/%{version}/Python-%{version}.tar.xz

%include /root/rpmbuild/SPECS/requires.spec

# When the user tries to `yum install python`, yum will list this package among
# the possible alternatives
Provides: alternative-for(python)

%description
Python is the magical programming language that we all like.

%prep
%setup -q -n Python-%{version}

%build
%include /root/rpmbuild/SPECS/build.spec

%install
%include /root/rpmbuild/SPECS/install.spec

%files
/usr/local/*
/etc/ld.so.conf.d/python%{pybasever}.conf

%post
# process ld path
ldconfig

%postun
# remove search path
ldconfig
